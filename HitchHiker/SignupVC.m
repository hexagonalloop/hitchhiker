//
//  SignupVC.m
//  HitchHiker
//
//  Created by Honey Jain on 7/11/15.
//  Copyright (c) 2015 Hexagonal Loop. All rights reserved.
//

#import "SignupVC.h"
#import "NSString+Validator.h"
#import "GKAPIManager.h"
#import "WCAlertView.h"
@interface SignupVC ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *btnTapForImage;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *genderButtons;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *formButtons;

@property (weak, nonatomic) IBOutlet UITextField *txtFname;
@property (weak, nonatomic) IBOutlet UITextField *txtLName;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtPass1;
@property (weak, nonatomic) IBOutlet UITextField *txtPass2;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
- (IBAction)handleSignUp:(id)sender;

- (IBAction)handleGenderToggle:(id)sender;
- (IBAction)handleChooseImage:(id)sender;



@property(nonatomic, strong)NSMutableArray *errFields;
@end

@implementation SignupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    switch (textField.tag - 100) {
        case SWNameKey:{
            [self.txtLName becomeFirstResponder];
        }
            break;
        case 10:{
            [self.txtPhoneNo becomeFirstResponder];
        }
            break;
            case SWPhoneNumberKey:{
            [self.txtEmail becomeFirstResponder];
        }
        case SWEmailKey:{
            [self.txtPass1 becomeFirstResponder];
        }
            break;
        case 8:{
            [self.txtPass2 becomeFirstResponder];
        }
            break;
            
            
        default:
            break;
    }
    return YES;
}
#pragma mark - TAbleViewDelegate Methods
-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%@", indexPath);
}

#pragma mark - Action Methods
- (IBAction)handleSignUp:(id)sender {
    BOOL isValid = [self validateForm];
    if (!isValid) {
        //alert user about the invalid entries
        [WCAlertView showAlertWithTitle:@"Validation Failed" message:@"Please correct the highlighted fields and try again." customizationBlock:nil completionBlock:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        return;
    }
    
    NSString *strName = [NSString stringWithFormat:@"%@ %@", self.txtFname.text, self.txtLName.text];

    [GKAPIManager signUpWithFirstName:strName
                                email: self.txtEmail.text
                                phone:self.txtPhoneNo.text
                                 pass:self.txtPass1.text
                               gender:[self gender]
                             callback:^(BOOL success, id result) {
                                 NSLog(@"------\n %@ \n---- ", result);
                             }];
    
    
    
    
}

- (IBAction)handleGenderToggle:(UIButton *)sender {
    for (UIButton *btn in self.genderButtons){
        [btn setSelected:!btn.selected];
    }
}

- (IBAction)handleChooseImage:(id)sender {
//    UL_ImagePickerManager *manager = [[UL_ImagePickerManager alloc]init];
//    SignupVC *__weak _weakSelf = self;
//    [manager showImagePickerWithDelegate:nil onSuccess:^(UIImage *image) {
//        [_weakSelf.imageView setImage:image];
//    } onFailureOrCancel:nil];

}

#pragma mark - conv Methods

-(BOOL)validateForm{
    BOOL flag = YES;
    if (!self.errFields) {
        _errFields = [[NSMutableArray alloc]initWithCapacity:4];
    }
    [_errFields removeAllObjects];
    for (UITextField *tf in self.formButtons) {

        switch (tf.tag-1000) {
            case SWEmailKey:
            {
                flag &= [tf.text isValidEmailAddress];
                [self.errFields addObject:@"Invalid Email."];
            }
                break;
            case SWNameKey:
            case 10:
            {
                flag &= [tf.text isValidName];
                [self.errFields addObject:@"Invalid Name."];
            }
                break;
            case SWPasswordKey:
                case 8:
            {
                flag &= [tf.text isValidPassword];
                [self.errFields addObject:@"Improper Password."];
            }
                break;
            case SWPhoneNumberKey:
            {
                flag &= [tf.text isValidNumeric];
                [self.errFields addObject:@"Invalid Phone number."];
            }
                break;
               default:
                break;
        }
    }
    
    flag &= [self.txtPass1.text isEqualToString:self.txtPass2.text];

    return flag;// || self.errFields.count == 0;
}

-(NSString *)gender{
    for (UIButton *btn in self.genderButtons) {
        if (btn.isSelected) {
            return btn.titleLabel.text.lowercaseString;
        }
    }
    return nil;
}
@end

//
//  AppDelegate.h
//  HitchHiker
//
//  Created by Gaurav Keshre on 7/11/15.
//  Copyright (c) 2015 Hexagonal Loop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
+(void)replaceCurrentViewControllerWith:(UIViewController *)newViewController;

@end


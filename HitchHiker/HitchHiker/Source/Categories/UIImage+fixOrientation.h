//
//  UIImage+fixOrientation.h
//  UsLikey
//
//  Created by Gourav Jamwal on 9/24/13.
//  Copyright (c) 2013 Softway Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees;
@end

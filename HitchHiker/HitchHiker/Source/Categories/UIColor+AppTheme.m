//
//  UIColor+AppTheme.m
//  GKZiffyDemo
//
//  Created by Gaurav Keshre on 6/27/15.
//  Copyright (c) 2015 Gaurav Keshre. All rights reserved.
//

#import "UIColor+AppTheme.h"

@implementation UIColor (AppTheme)
+(UIColor *)appLightBlueColor{
    return [UIColor colorWithRed:0.333 green:0.776 blue:0.769 alpha:1.000];
}
+(UIColor *)appDarkBlueColor{
    return [UIColor colorWithRed:0.110 green:0.671 blue:0.663 alpha:1.000];
}
+(UIColor *)appBarColor{
    return [UIColor colorWithRed:0.039 green:0.098 blue:0.161 alpha:1.000];
}
+(UIColor *)appRed{
    return [UIColor colorWithRed:0.851 green:0.004 blue:0.255 alpha:1.000]
    ;
}

+(UIColor *)appLightGrey{
    return [UIColor colorWithRed:0.878 green:0.882 blue:0.890 alpha:1.000];
}

#pragma mark - HitchHacker Methods
+(UIColor *)appVioletColor{
    return [UIColor colorWithRed:0.075 green:0.047 blue:0.137 alpha:1.000];
}
+(UIColor *)appLigtVioletColor{
    return [UIColor colorWithRed:0.278 green:0.157 blue:0.400 alpha:1.000];
}

+(UIColor *)appPinkColor{
    return [UIColor colorWithRed:0.773 green:0.290 blue:0.439 alpha:1.000];
}
+(UIColor *)appPurpleColor{
    return [UIColor colorWithRed:0.478 green:0.157 blue:0.353 alpha:1.000];
}
+(UIColor *)appBrownColor{
    return [UIColor colorWithRed:0.404 green:0.224 blue:0.180 alpha:1.000];
}
+(UIColor *)appLightBrownColor{
    return [UIColor colorWithRed:0.776 green:0.576 blue:0.463 alpha:1.000];
}
+(UIColor *)appGreyColor{
    return [UIColor colorWithRed:0.463 green:0.455 blue:0.471 alpha:1.000];
}

+(UIColor *)appBlackColor{
    return [UIColor colorWithRed:0.133 green:0.137 blue:0.157 alpha:1.000];
}





@end

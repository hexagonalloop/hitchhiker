// UIImage+Resize.h
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

// Extends the UIImage class to support resizing/cropping
#import <UIKit/UIKit.h>
@interface UIImage (Resize)

- (UIImage *)croppedImage:(CGRect)cropRect;

- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality
      withDeviceOrientation:(UIDeviceOrientation)deviceOrientation;

- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;

//gk
- (UIImage *)imageThumbnail;
- (UIImage *)rotateAppropriately;
- (UIImage *)resizeWithMaxAllowedSize:(CGSize)newSize;
- (UIImage *)compressToMaxSize:(CGSize)maxSize;


//manish
- (UIImage*) scaleImageToSize:(CGSize)newSize;
-(UIImage *)resizedImageWithImage:(UIImage *)image;
-(UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
//gk
+ (UIImage *)thumbnailFromImage:(UIImage *)_image;
+ (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image maxWidth:(NSUInteger)width;
-(UIImage *)cropImage:(UIImage *)sourceImage WithMainRect:(CGRect)mainRect andCroppingFrame:(CGRect)imageCroppingFrame;
@end

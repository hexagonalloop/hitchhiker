// UIImage+Resize.m
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

#import "UIImage+Resize.h"
//#import "UIImage+RoundedCorner.h"
//#import "UIImage+Alpha.h"

// Private helper methods
@interface UIImage ()
- (UIImage *)resizedImage:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality;
- (CGAffineTransform)transformForOrientation:(CGSize)newSize;
@end

@implementation UIImage (Resize)


- (UIImage *)rotateAppropriately{
    CGImageRef imageRef = [self CGImage];
    UIImage *properImage;
    
    if ([self imageOrientation]==UIImageOrientationUp) {
        properImage = self;
    }
    
    else if ([self imageOrientation]==UIImageOrientationLeft){
        properImage= [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationUp];
        
    }
    
    //    else if ([self imageOrientation]==UIImageOrientationRight){
    //        properImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationRight];
    //
    //    }else if ([self imageOrientation]==UIImageOrientationDown){
    //        properImage = [UIImage imageWithCGImage:imageRef scale:1.0 orientation:UIImageOrientationDown];
    //
    //    }
    //   CGImageRelease(imageRef);
    return properImage;
}

- (UIImage *)resizeWithMaxAllowedSize:(CGSize)newSize{
    
    if (CGSizeEqualToSize(self.size, newSize))return self;
    if (self.size.width <= newSize.width && self.size.height <= newSize.height) return self;
    
    
    
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

// Returns a copy of this image that is cropped to the given bounds.
// The bounds will be adjusted using CGRectIntegral.
// This method ignores the image's imageOrientation setting.
- (UIImage *)croppedImage:(CGRect)cropRect {
    
    if (self.size.height < cropRect.size.height ) {
        UIImage *image = [self thumbnailImage:cropRect.size.height transparentBorder:0 cornerRadius:0 interpolationQuality:9];
        return image;
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

// Returns a copy of this image that is squared to the thumbnail size.
// If transparentBorder is non-zero, a transparent border of the given size will be added around the edges of the thumbnail. (Adding a transparent border of at least one pixel in size has the side-effect of antialiasing the edges of the image when rotating it using Core Animation.)
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality {
    UIImage *resizedImage = [self resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                       bounds:CGSizeMake(thumbnailSize, thumbnailSize)
                                         interpolationQuality:quality];
    
    // Crop out any part of the image that's larger than the thumbnail size
    // The cropped rect must be centered on the resized image
    // Round the origin points so that the size isn't altered when CGRectIntegral is later invoked
    CGRect cropRect = CGRectMake(round((resizedImage.size.width - thumbnailSize) / 2),
                                 round((resizedImage.size.height - thumbnailSize) / 2),
                                 thumbnailSize,
                                 thumbnailSize);
    UIImage *croppedImage = [resizedImage croppedImage:cropRect];
    
    return croppedImage;
    //    UIImage *transparentBorderImage = borderSize ? [croppedImage transparentBorderImage:borderSize] : croppedImage;
    //
    //    return [transparentBorderImage roundedCornerImage:cornerRadius borderSize:borderSize];
}
//hemanth
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality
 withDeviceOrientation:(UIDeviceOrientation)deviceOrientation{
    UIImage *resizedImage = [self resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                       bounds:CGSizeMake(thumbnailSize, thumbnailSize)
                                         interpolationQuality:quality];
    
    // Crop out any part of the image that's larger than the thumbnail size
    // The cropped rect must be centered on the resized image
    // Round the origin points so that the size isn't altered when CGRectIntegral is later invoked
    
    CGFloat x=round((resizedImage.size.width - thumbnailSize) / 2);
    CGFloat y=round((resizedImage.size.height - thumbnailSize) / 2);
    CGFloat width=thumbnailSize;
    CGFloat height=thumbnailSize;
    
    	if ([[[UIDevice currentDevice] systemVersion] integerValue] >= 7) {
            if (deviceOrientation==UIDeviceOrientationFaceDown) {
                x=x+25;
                width=width-10;
                height=height-55;
            }
            else if (deviceOrientation==UIDeviceOrientationFaceUp)
            {
                x=x+25;
                width=width-10;
                height=height-55;
            }
            else if(deviceOrientation==UIDeviceOrientationPortrait)
            {
                x=x+25;
                width=width-10;
                height=height-55;
            }
            else if(deviceOrientation==UIDeviceOrientationLandscapeLeft)
            {
                y=y+10;
                width=width-20;
                height=height-10;
            }
            else if(deviceOrientation==UIDeviceOrientationLandscapeRight)
            {
                x=x+15;
                y=y+10;
                height=height-5;
            }
            else if (deviceOrientation==UIDeviceOrientationPortraitUpsideDown)
            {
                x=x+10;
                y=y+15;
                height=height-50;
                width=width-15;
            }
            else
            {
                x=x-5;
                y=y+15;
                width=width-55;
                height=height-50;
            }
        }
    else
    {
        if (deviceOrientation==UIDeviceOrientationFaceDown) {
            x=x+15;
            y=y-5;
            width=width-40;
            height=height-45;
        }
        else if (deviceOrientation==UIDeviceOrientationFaceUp)
        {
            x=x+15;
            y=y-5;
            width=width-40;
            height=height-45;
        }
        else if(deviceOrientation==UIDeviceOrientationPortrait)
        {
            x=x+20;
            y=y-5;
            width=width-40;
            height=height-45;
        }
        else if(deviceOrientation==UIDeviceOrientationLandscapeLeft)
        {
            x=x-5;
            y=y+20;
            width=width-50;
            height=height-40;
        }
        else if(deviceOrientation==UIDeviceOrientationLandscapeRight)
        {
            x=x+55;
            y=y+25;
            height=height-45;
            width=width-50;
        }
        else if (deviceOrientation==UIDeviceOrientationPortraitUpsideDown)
        {
            x=x+35;
            y=y+55;
            height=height-50;
            width=width-25;
        }
        else
        {
            x=x-5;
            y=y+15;
            width=width-55;
            height=height-50;
        }
    }
    CGRect cropRect = CGRectMake(x,y,width,height);
    UIImage *croppedImage = [resizedImage croppedImage:cropRect];
    
    return croppedImage;
    //    UIImage *transparentBorderImage = borderSize ? [croppedImage transparentBorderImage:borderSize] : croppedImage;
    //
    //    return [transparentBorderImage roundedCornerImage:cornerRadius borderSize:borderSize];
}
//hemanth
-(UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a bitmap context.
   // UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(UIImage *)resizedImageWithImage:(UIImage *)image
{
    if (image.size.width<306 || image.size.height<306) {
        return [self imageWithImage:image scaledToSize:CGSizeMake(306, 306)];
    }
    if (image.size.width<612 || image.size.height<612) {
        if (image.size.width<image.size.height) {
            return [self imageWithImage:image scaledToSize:CGSizeMake(612, 612)];
        }
        else
        {
             return [self imageWithImage:image scaledToSize:CGSizeMake(image.size.height, image.size.height)];
        }
    }
    else if (image.size.width<1024 || image.size.height<1024) {
        if (image.size.width<image.size.height) {
            return [self imageWithImage:image scaledToSize:CGSizeMake(image.size.width, image.size.width)];
        }
        else
        {
            return [self imageWithImage:image scaledToSize:CGSizeMake(image.size.height, image.size.height)];
        }
    }
    else
        return [self imageWithImage:image scaledToSize:CGSizeMake(1024, 1024)];
}
// Returns a rescaled copy of the image, taking into account its orientation
// The image will be scaled disproportionately if necessary to fit the bounds specified by the parameter
- (UIImage *)resizedImage:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality {
    BOOL drawTransposed;
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            drawTransposed = YES;
            break;
            
        default:
            drawTransposed = NO;
    }
    
    return [self resizedImage:newSize
                    transform:[self transformForOrientation:newSize]
               drawTransposed:drawTransposed
         interpolationQuality:quality];
}

// Resizes the image according to the given content mode, taking into account the image's orientation
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality {
    
    CGFloat horizontalRatio = bounds.width / self.size.width;
    CGFloat verticalRatio = bounds.height / self.size.height;
    CGFloat ratio;
    
    switch (contentMode) {
        case UIViewContentModeScaleAspectFill:
            ratio = MAX(horizontalRatio, verticalRatio);
            break;
            
        case UIViewContentModeScaleAspectFit:
            ratio = MIN(horizontalRatio, verticalRatio);
            break;
            
        default:
            [NSException raise:NSInvalidArgumentException format:@"Unsupported content mode: %d", contentMode];
    }
    
    CGSize newSize = CGSizeMake(self.size.width * ratio, self.size.height * ratio);
    
    return [self resizedImage:newSize interpolationQuality:quality];
}

#pragma mark - IMAGE Resize @Manish
- (UIImage*) scaleImageToSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( self.size.width > self.size.height ) {
        scaleFactor = self.size.width / self.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = self.size.height / self.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContext(scaledSize);
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [self drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
-(UIImage *)compressToMaxSize:(CGSize)maxSize{
    float actualHeight = self.size.height;
    float actualWidth = self.size.width;
    float maxHeight = maxSize.height;// 600.0;
    float maxWidth = maxSize.width; //800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    //    float compressionQuality = compressionQulality ;//0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    if (actualHeight<306) {
        actualHeight=306;
        actualWidth=actualWidth+306*imgRatio;
    }
    if (actualWidth<306) {
        actualWidth=306;
        actualHeight=actualHeight+306*imgRatio;
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [self drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    //    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    return img;// [UIImage imageWithData:imageData];
}


#pragma mark -
#pragma mark Private helper methods
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

// Returns a copy of the image that has been transformed using the given affine transform and scaled to the new size
// The new image's orientation will be UIImageOrientationUp, regardless of the current image's orientation
// If the new size is not integral, it will be rounded up
- (UIImage *)resizedImage:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectMake(0, 0, newRect.size.height, newRect.size.width);
    CGImageRef imageRef = self.CGImage;
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                0,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}



// Returns an affine transform that takes into account the image orientation when drawing a scaled image
- (CGAffineTransform)transformForOrientation:(CGSize)newSize {
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:           // EXIF = 3
        case UIImageOrientationDownMirrored:   // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:           // EXIF = 6
        case UIImageOrientationLeftMirrored:   // EXIF = 5
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:          // EXIF = 8
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:     // EXIF = 2
        case UIImageOrientationDownMirrored:   // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:   // EXIF = 5
        case UIImageOrientationRightMirrored:  // EXIF = 7
            transform = CGAffineTransformTranslate(transform, newSize.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
            
    }
    
    return transform;
}
#pragma clang diagnostic pop
#pragma mark -
-(UIImage *)imageThumbnail{
    if (self ==nil)return self;
	// Create a thumbnail version of the image for the event object.
	CGSize size = self.size;
	CGSize croppedSize;
	CGFloat ratio_w = 88.0;
	CGFloat ratio_h = 88.0;
	CGFloat offsetX = 0.0;
	CGFloat offsetY = 0.0;
	
	// check the size of the image, we want to make it
	// a square with sides the size of the smallest dimension
	if (size.width > size.height) {
		offsetX = (size.height - size.width) / 2;
		croppedSize = CGSizeMake(size.height, size.height);
	} else {
		offsetY = (size.width - size.height) / 2;
		croppedSize = CGSizeMake(size.width, size.width);
	}
	
	// Crop the image before resize
	CGRect clippedRect = CGRectMake(offsetX * -1, offsetY * -1, croppedSize.width, croppedSize.height);
	CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], clippedRect);
	// Done cropping
	
	// Resize the image
	CGRect rect = CGRectMake(0.0, 0.0, ratio_w, ratio_h);
	
	UIGraphicsBeginImageContext(rect.size);
	[[UIImage imageWithCGImage:imageRef] drawInRect:rect];
	UIImage *_thumbnail = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	// Done Resizing
	CFRelease(imageRef);
	return _thumbnail;
}

#pragma mark - Thumbnail

+(UIImage *)thumbnailFromImage:(UIImage *)_image{
    if (_image ==nil)return _image;
	// Create a thumbnail version of the image for the event object.
	CGSize size = _image.size;
	CGSize croppedSize;
	CGFloat ratio_w = 212.0;
	CGFloat ratio_h = 159.0;
	CGFloat offsetX = 0.0;
	CGFloat offsetY = 0.0;
	
	// check the size of the image, we want to make it
	// a square with sides the size of the smallest dimension
	if (size.width > size.height) {
		offsetX = (size.height - size.width) / 2;
		croppedSize = CGSizeMake(size.height, size.height);
	} else {
		offsetY = (size.width - size.height) / 2;
		croppedSize = CGSizeMake(size.width, size.width);
	}
	
	// Crop the image before resize
	CGRect clippedRect = CGRectMake(offsetX * -1, offsetY * -1, croppedSize.width, croppedSize.height);
	CGImageRef imageRef = CGImageCreateWithImageInRect([_image CGImage], clippedRect);
	// Done cropping
	
	// Resize the image
	CGRect rect = CGRectMake(0.0, 0.0, ratio_w, ratio_h);
	
	UIGraphicsBeginImageContext(rect.size);
	[[UIImage imageWithCGImage:imageRef] drawInRect:rect];
	UIImage *_thumbnail = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	// Done Resizing
	CFRelease(imageRef);
	return _thumbnail;
}

+ (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
#pragma mark - rotation Methods
+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = FLT_MAX; // Or whatever
    return [UIImage scaleAndRotateImage:image maxWidth:kMaxResolution];
}
+ (UIImage *)scaleAndRotateImage:(UIImage *)image maxWidth:(NSUInteger)kMaxResolution{
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
-(UIImage *)cropImage:(UIImage *)sourceImage WithMainRect:(CGRect)mainRect andCroppingFrame:(CGRect)imageCroppingFrame
{
    UIImage *transform;
    CGFloat deltaWidth=sourceImage.size.width/mainRect.size.width;
    CGFloat deltaHeight=sourceImage.size.height/mainRect.size.height;
    CGRect croppingFrame=CGRectMake(imageCroppingFrame.origin.x*deltaWidth, imageCroppingFrame.origin.y*deltaHeight, imageCroppingFrame.size.width*deltaWidth, imageCroppingFrame.size.height*deltaHeight);
    if ([[[UIDevice currentDevice] systemVersion] integerValue] < 7)
    {
        if ([[UIScreen mainScreen] bounds].size.height<568)
        {
            transform=  [sourceImage croppedImage:CGRectMake(croppingFrame.origin.x, croppingFrame.origin.y+12*deltaHeight, croppingFrame.size.width, croppingFrame.size.height+35*deltaHeight)];
        }
        else
        {
            transform=  [sourceImage croppedImage:CGRectMake(croppingFrame.origin.x, croppingFrame.origin.y+14*deltaHeight, croppingFrame.size.width, croppingFrame.size.height+(100*deltaHeight))];
        }
    }
    else
    {
        if ([[UIScreen mainScreen] bounds].size.height>480) {
            transform=  [sourceImage croppedImage:CGRectMake(croppingFrame.origin.x, croppingFrame.origin.y-(56*deltaHeight), croppingFrame.size.width, croppingFrame.size.height+(90*deltaHeight))];
        }
        else
        {
            transform=  [sourceImage croppedImage:CGRectMake(croppingFrame.origin.x, croppingFrame.origin.y-17*deltaHeight, croppingFrame.size.width, croppingFrame.size.height+(32*deltaHeight))];
        }
    }
    return transform;
}


#pragma mark - April 3 Methods

-(UIImage *)captureScreenInRect:(CGRect)captureFrame wrtView:(UIView *)view{
    return [self cropImage:self WithMainRect:view.bounds andCroppingFrame:captureFrame];
    
    CALayer *layer;
    layer = view.layer;
    UIGraphicsBeginImageContext(captureFrame.size);
    CGContextClipToRect (UIGraphicsGetCurrentContext(),captureFrame);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}
@end

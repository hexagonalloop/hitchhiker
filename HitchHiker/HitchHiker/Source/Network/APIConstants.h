//
//  APIConstants.h
//  GKZiffyDemo
//
//  Created by Gaurav Keshre on 6/28/15.
//  Copyright (c) 2015 Gaurav Keshre. All rights reserved.
//


#define HOST @"http://10.10.10.91:8000/api"
#define API_URL(controller,action) HOST @"/" controller @"/" action

/*
 * CONTRO MACRO
 */
#define CONT_USER   @"user"

/*
 * ACTION MACRO
 */
#define ACTION_SEARCH   @"search"
#define ACTION_LOGIN    @"login"
#define ACTION_SIGNUP   @"signup"
#define ACTION_PINVerify   @"verify_pin"
/*
 * PARAMS MACRO
 */
#define PARAM_CITY_ID   @"city_id"
#define PARAM_VERTICAL  @"vertical"
#define PARAM_QUERY     @"q"
#define PARAM_PAGE      @"page"


/*
 * PARAMS GEENRATOR
 */
#define API_SEARCH_PARAMS(city_id, vertical, query, page) \
@{PARAM_CITY_ID:city_id, PARAM_VERTICAL:vertical, PARAM_QUERY:query, PARAM_PAGE:page}
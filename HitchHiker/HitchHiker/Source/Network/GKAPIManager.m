//
//  GKAPIManager.m
//  GKZiffyDemo
//
//  Created by Gaurav Keshre on 6/27/15.
//  Copyright (c) 2015 Gaurav Keshre. All rights reserved.
//

#import "GKAPIManager.h"
#import "GKHTTPRequester.h"
#import "APIConstants.h"
#import "NSDictionary+Validation.h"

@interface GKAPIManager ()
    @property (strong, nonatomic)GKHTTPRequester *requester;
@end

@implementation GKAPIManager


+(instancetype)instance{
    static GKAPIManager *instance;
static dispatch_once_t onceToken;
dispatch_once(&onceToken, ^{
    instance = [GKAPIManager new];
});
    return instance;
}

-(void)dealloc{
    _requester = nil;

}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requester = [GKHTTPRequester new];
        [_requester setMethod:GKHTTPMethodPOST];
    }
    return self;
}


#pragma mark - Instnace Methods

-(void)signUpWithFirstName:(NSString *)fname
                    email:(NSString *)email
                    phone:(NSString*)phoneNo
                     pass:(NSString *)pass
                   gender:(NSString *)g
                  callback:(GKSuccessCallback)callback{
    
    NSString *strURL = API_URL(CONT_USER, ACTION_SIGNUP);
    NSDictionary *param = @{@"email":email, @"name":fname, @"password":pass, @"gender":g, @"mobile":phoneNo};

    [self.requester invokeURL:strURL withParams:param callback:callback];
    
}
//name', 'email', 'password','mobile', 'gender', 'password_confirmation'

-(void)loginWithEmail:(NSString *)email
                      pass:(NSString *)pass callback:(GKSuccessCallback)callback{
    NSString *strURL = API_URL(CONT_USER, ACTION_LOGIN);
    NSDictionary *param = @{@"email":email,@"password":pass};
    
    [self.requester invokeURL:strURL withParams:param callback:callback];

    
}
-(void)verifyPIN:(NSString *)PIN callback:(GKSuccessCallback)callback{
    NSString *strURL = API_URL(CONT_USER, ACTION_PINVerify);
    NSDictionary *param = @{@"pin":PIN,};
    
    [self.requester invokeURL:strURL withParams:param callback:callback];
    
}
#pragma mark - Public Methods
+(void)signUpWithFirstName:(NSString *)fname
                    email:(NSString *)email
                    phone:(NSString*)phoneNo
                     pass:(NSString *)pass
                   gender:(NSString *)g
                  callback:(GKSuccessCallback)callback{
    [[GKAPIManager instance] signUpWithFirstName:fname email:email phone:phoneNo pass:pass gender:g callback:callback];
}
+(void)loginWithEmail:(NSString *)email
                 pass:(NSString *)pass
             callback:(GKSuccessCallback)callback{
    [[GKAPIManager instance] loginWithEmail:email pass:pass callback:callback];
}
+(void)verifyPIN:(NSString *)PIN
        callback:(GKSuccessCallback)callback{
    [[GKAPIManager instance] verifyPIN:PIN callback:callback];
    
}
@end

//
//  GKAPIManager.h
//  GKZiffyDemo
//
//  Created by Gaurav Keshre on 6/27/15.
//  Copyright (c) 2015 Gaurav Keshre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GKHTTPRequester.h"
@interface GKAPIManager : NSObject
+(void)signUpWithFirstName:(NSString *)fname
                     email:(NSString *)email
                     phone:(NSString*)phoneNo
                      pass:(NSString *)pass
                    gender:(NSString *)g
                  callback:(GKSuccessCallback)callback;

+(void)loginWithEmail:(NSString *)email
                 pass:(NSString *)pass
             callback:(GKSuccessCallback)callback;

+(void)verifyPIN:(NSString *)PIN
        callback:(GKSuccessCallback)callback;

    
@end


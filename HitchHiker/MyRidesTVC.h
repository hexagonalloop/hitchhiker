//
//  MyRidesTVC.h
//  HitchHiker
//
//  Created by Honey Jain on 7/11/15.
//  Copyright (c) 2015 Hexagonal Loop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRidesTVC : UITableViewController
- (IBAction)handleHomeButton:(id)sender;

@end
